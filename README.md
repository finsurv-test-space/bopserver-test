# bopServer

A server POC for the bop form.

## Project layout

This POC consists of 3 components:
    . The Express App which serves the bop API script
    . The API scripts.
    . The test harness to test the serving of the API script and embedding of the form.

## Testing the environment

Run the express server:
```
>>npm start
```

## TODO Actionable items

    . API-specialised builds
        . API should take in a feature-list of features to be disabled. no feature-list = complete-build


## StdBank Spaces  
CF = ClientFacing
BO = BackOffice

    . General
        . Document that answers the question: "What data/fields do we need from you?"


    . Hoops BO
        . Java Application 
            . Client Java 6 (1.6)
            . Server Java 8 (1.8)
        . PLEASE MAIN THE ORWEL and JSON SCHEMA to Brian!!



    . BANCS









    . RFS
    . EE CF BO
    . nBOL CF