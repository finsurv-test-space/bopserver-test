
var gulp = require('gulp');
var list = require('gulp-task-listing');
var requireDir = require('require-dir');
var FwdRef = require('undertaker-forward-reference');
 
gulp.registry(FwdRef()); // or gulp.registry(new FwdRef());
 
// Require all tasks in gulp/tasks, including subfolders
requireDir('./gulp/tasks', { recurse: true });

gulp.task('default', list);