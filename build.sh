#!/bin/bash

ROOT=$PWD

DATETIME=$(date +"%Y-%m-%d_(%H-%M)")
write_metadata(){
   echo "{
        	\"artefact\":
                []
        }" > ${ROOT}/build/metadata.json

}

# Submodules shouldn't have to be checked-out each time a build is done
# git submodule update --init --recursive

npm install 


# Remove And Create All Build Folders
rm -rf dev/html
rm -rf dist/

if [ -f "build/metadata.json" ]; then
      rm build/metadata.json
fi

# Create Build Directory
mkdir -p dist/html
mkdir -p dev/html
mkdir -p dist/js/pack

cd electronic-bop-form/
npm install
npm run build #xxx warning: recursive invocation
cd ${ROOT}

node ./node_modules/gulp/bin/gulp.js build
node ./node_modules/gulp/bin/gulp.js minify

cp testHarness/channel.html dev/html/
cp testHarness/channel.html dist/html/

# copy channel packs

cd ./electronic-bop-form/build/rules/crules/
for d in */ ; do (
    echo 'copying package:' $d
    mkdir -p ${ROOT}/dist/js/pack/$d
    cp $d/pack.min.js ${ROOT}/dist/js/pack/$d/
); 
done;

cd ${ROOT}

if [ ! -d 'build' ]; then
     mkdir build
fi

cp -r dist build/dist_${DATETIME}
write_metadata
