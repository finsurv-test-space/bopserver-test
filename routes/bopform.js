var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');

var config = require('./config.json');

router.get('/ping/:message', function(req, res) {
  res.set('Content-Type', 'application/text');
  res.send(`pingback: ${req.params.message}`);
});

router.post('/', function(req, res, next) {
  console.log(`got ${JSON.stringify(req.body)}`);
  store.push(req.body);
  res.status(200);
  res.send();
});

function readPromise(fileName) {
  return new Promise((resolve, reject) => {
    fs.readFile(fileName, 'utf8', (err, data) => {
      if (err) reject(err);
      else resolve(data);
    });
  });
}

router.get('/lookups/:channelID', function(req, res) {
  readPromise(
    path.join(
      config.compiledRepoPath,
      req.params.channelID,
      'data',
      'lookups.js'
    )
  )
    .then(lookups => lookups.replace(/^define\(/, 'define("data/lookups",'))
    .then(lookups => {
      res.set('Content-Type', 'application/javascript');
      res.send(lookups);
    });
});

router.get('/lookup/:channelID/:lookupName', function(req, res) {
  var lookups = Require(
    path.join(
      config.compiledRepoPath,
      req.params.channelID,
      'data',
      'lookups.js'
    )
  );
  res.set('Content-Type', 'application/javascript');
  res.send(JSON.stringify(lookups[req.params.lookupName]));
});

// does not package the lookups...
router.get('/getRulePack/:channelID', function(req, res, next) {
  res.sendFile(req.params.channelID + '/pack.min.js', {
    root: config.compiledRepoPath
  });
});

router.get('/reports/sarb/:channelID', function(req, res, next) {
  let trnReference = req.query['trnReference'];

  res.set('Content-Type', 'application/json');

  if (trnReference != null) {
    readPromise(`${config.dataPath}${trnReference}.json`)
      .then(fileData => {
        res.send(fileData);
      })
      .catch(err => {
        res.send(store[1]);
      });
  } else {
    res.send(store[1]);
  }
});

router.get('/form/js/:channelID', function(req, res, next) {
  //'evaluation/evalScenarios',
  //'evaluation/evalRules',
  //'validation/importexport',
  //'validation/money',
  //'validation/transaction',

  Promise.all([
    readPromise(path.join(config.jsPath, 'lib.js')),
    readPromise(path.join(config.compiledRepoPath, req.params.channelID, 'data', 'lookups.js'))
      .then(fileContent => fileContent.replace(/define\(/, 'define("data/lookups",')),
    readPromise(path.join(config.compiledRepoPath, req.params.channelID, 'evaluation', 'evalScenarios.js'))
      .then(fileContent => fileContent.replace(/define\(/, 'define("evaluation/evalScenarios",')),
    readPromise(path.join(config.compiledRepoPath, req.params.channelID, 'evaluation', 'evalContext.js'))
      .then(fileContent => fileContent.replace(/define\(/, 'define("evaluation/evalContext",')),
    readPromise(path.join(config.compiledRepoPath, req.params.channelID, 'evaluation', 'evalRules.js'))
      .then(fileContent => fileContent.replace(/define\(/, 'define("evaluation/evalRules",')),
    readPromise(path.join(config.compiledRepoPath, req.params.channelID, 'validation', 'importexport.js'))
      .then(fileContent => fileContent.replace(/define\(/, 'define("validation/importexport",')),
    readPromise(path.join(config.compiledRepoPath, req.params.channelID, 'validation', 'money.js'))
      .then(fileContent => fileContent.replace(/define\(/, 'define("validation/money",')),
    readPromise(path.join(config.compiledRepoPath, req.params.channelID, 'validation', 'transaction.js'))
      .then(fileContent => fileContent.replace(/define\(/, 'define("validation/transaction",')),
    
    readPromise(path.join(config.compiledRepoPath, req.params.channelID, 'document', 'importexport.js'))
      .then(fileContent => fileContent.replace(/define\(/, 'define("document/importexport",')),
    readPromise(path.join(config.compiledRepoPath, req.params.channelID, 'document', 'money.js'))
      .then(fileContent => fileContent.replace(/define\(/, 'define("document/money",')),
    readPromise(path.join(config.compiledRepoPath, req.params.channelID, 'document', 'transaction.js'))
      .then(fileContent => fileContent.replace(/define\(/, 'define("document/transaction",')),

    readPromise(path.join(config.compiledRepoPath, req.params.channelID, 'pack.min.js')),
    readPromise(path.join(config.jsPath, 'main.js'))
  ])
    .then(content => {
      console.log(typeof content[0]);
      return content.reduce((memo, item) => memo.concat(item), '');
    })
    .then(api => {
      res.set('Content-Type', 'application/javascript');

      res.send(api);
    })
    .catch(err => {
      console.log(err);
    });
});

router.get('/form/:channelID', function(req, res, next) {
  let trnReference = req.query['trnReference'];
  let allowNew = req.query['allowNew'];
  let proxyPath = req.query['proxyPath'];
  if (trnReference != null) {
    readPromise(`${config.dataPath}${trnReference}.json`)
      .then(fileData => {
        res.render('bopform', {
          system: req.params.channelID,
          data: JSON.parse(fileData)
        });
      })
      .catch(err => {
        res.render('bopform', { system: req.params.channelID, data: store[1] });
      });
  } else {
    res.render('bopform', { system: req.params.channelID, data: store[1] });
  }
});

router.get('/decision/evaluateCustomerPayment', function(req, res) {
  var channelName = req.query.channelName;
  var drResStatus = req.query.drResStatus;
  var drAccType = req.query.drAccType;
  var beneficiaryInstitutionBIC = req.query.beneficiaryInstitutionBIC;
  var beneficiaryCurrency = eq.query.beneficiaryCurrency;
});

router.get('/barejs/:channelID', function(req, res, next) {
  Promise.all([
    readPromise(path.join(config.jsPath, 'lib.js')),
    readPromise(
      path.join(config.compiledRepoPath, req.params.channelID, 'pack.min.js')
    ),
    readPromise(path.join(config.jsPath, 'main.js'))
  ])
    .then(content => {
      console.log(typeof content[0]);
      return content.reduce((memo, item) => memo.concat(item), '');
    })
    .then(api => {
      res.set('Content-Type', 'application/javascript');

      res.send(api);
    })
    .catch(err => {
      console.log(err);
    });
});

module.exports = router;

//The amazing in-memory transaction store

store = [
  {
    transaction: {
      Version: 'FINSURV',
      TrnReference: 'CTID:090f51e18325a515',
      ReportingQualifier: 'BOPCUS',
      Flow: 'OUT',
      ValueDate: '2017-02-28',
      FlowCurrency: 'USD',
      TotalForeignValue: 80,
      BranchCode: '99030100',
      BranchName: 'SANDTON',
      OriginatingBank: 'IVESZAJJXXX',
      OriginatingCountry: 'ZA',
      ReceivingBank: 'MCBLMUMUXXX',
      ReceivingCountry: 'MU',
      ReplacementTransaction: 'N',
      Resident: {
        Entity: {
          EntityName: 'Efdefwfl Vyvhhb Sytcpikvc (Vgv) Jtz',
          TradingName: 'Efdefwfl Vyvhhb Sytcpikvc (Vgv) Jtz',
          RegistrationNumber: '69749993672337',
          InstitutionalSector: '02',
          IndustrialClassification: '08',
          AccountName: 'FUE AWI Aipssfhj Pzfkka Skqktzusk',
          AccountIdentifier: 'RESIDENT OTHER',
          AccountNumber: '5631031997449',
          TaxNumber: '5954037741',
          VATNumber: '4043790584',
          TaxClearanceCertificateIndicator: 'N',
          StreetAddress: {
            Province: 'GAUTENG',
            AddressLine1: '144 Ksfjxgtm Seged',
            AddressLine2: 'Fjvfqox',
            Suburb: 'Fjvfqox',
            City: 'Kibyixikmhlc',
            PostalCode: '9734'
          },
          PostalAddress: {
            Province: 'GAUTENG',
            AddressLine1: 'KU Jqb 445',
            Suburb: 'Kibyixikmhlc',
            City: 'Kcqsbvpixt',
            PostalCode: '6938'
          },
          ContactDetails: {
            ContactName: 'Wdvaud',
            ContactSurname: 'Oehyuv',
            Email: 'ikiuxk.ekvhii@xgedvpjf.tx.vm',
            Telephone: '2499945557'
          }
        },
        Description:
          'Efdefwfl Vyvhhb Sytcpikvc (Vgv) Jtz, t/a Efdefwfl Vyvhhb Sytcpikvc (Vgv) Jtz'
      },
      NonResident: {
        Entity: {
          EntityName: 'RXTXJE MDG',
          AccountIdentifier: 'NON RESIDENT OTHER',
          AccountNumber: 'MZ47FRWH9583358940477566743',
          Address: { Country: 'MU' }
        }
      },
      MonetaryAmount: [
        {
          MoneyTransferAgentIndicator: null,
          ForeignValue: 80,
          SARBAuth: { RulingsSection: 'B.9(C)' },
          CategoryCode: '271',
          CategorySubCode: '04',
          LocationCountry: 'MU',
          SequenceNumber: 1,
          Category: '271/04',
          Description: 'Payment for freight services - air'
        }
      ]
    },
    customData: {
      DealerType: 'AD',
      ShowOldCodes: true,
      AccountDrCr: 'DR'
    }
  },
  {
    transaction: {
      Version: 'FINSURV',
      ReportingQualifier: 'BOPCUS',
      Flow: 'OUT',
      Resident: {
        Entity: {
          AccountNumber: '1100203024194',
          EntityName: 'Compo Agencies',
          TradingName: '',
          RegistrationNumber: '1973/003642/07',
          TaxNumber: '9135070846',
          VATNumber: '4410119830',
          StreetAddress: {
            AddressLine1: 'ypbyoitbmk',
            Suburb: 'Western Extension',
            City: 'Benoni',
            Province: 'Benoni',
            PostalCode: '1501',
            Country: 'South Africa'
          },
          ContactDetails: {
            ContactSurname: 'eeXXXXre',
            ContactName: 'viXXXXav',
            Email: 'JUciL@someweb.co.za',
            Telephone: '0115558228',
            Fax: ''
          },
          TaxClearanceCertificateIndicator: 'N'
        },
        Type: 'Entity'
      },
      NonResident: {
        Entity: { EntityName: 'anita', Address: { Country: 'AD' } }
      },
      MonetaryAmount: [
        {
          ImportExport: [],
          SARBAuth: {},
          TravelMode: {},
          ForeignValue: '12340.0',
          SequenceNumber: '1',
          MoneyTransferAgentIndicator: 'AD'//,
         // CategoryCode: '401'
        }
      ],
      ReceivingCountry: 'ZA',
      FlowCurrency: 'USD',
      OriginatingBank: '',
      TotalForeignValue: '12340.0',
      TrnReference: '090f4a1180755759',
      OriginatingCountry: '',
      ValueDate: '2018-02-07',
      ReceivingBank: 'IVESZAJJ0',
      LocationCountry: 'AD'
    },
    customData: {
      DealerType: 'AD'
    }
  }
];
