var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/ping/:message', function(req, res) {
  res.set('Content-Type', 'application/text');
  res.send(`pingback: ${req.params.message}`);
});

module.exports = router;
