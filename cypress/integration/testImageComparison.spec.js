/// <reference types="Cypress" />


//------------------ TEST TAB SWITCHING -------------------------
// describe('Compare', function () {
//   it('Should match screenshot', function () {
//     cy.visit('http://localhost:3010/report-data-store/producer/api/form/sbNA');
// cy.matchScreenshot('My Screenshot');
// cy.get('button').contains('Add monetary amount').click();
// cy.contains('Add monetary amount').click();

// cy.get('.uib-tab').first().click();

// cy.pause();
//     cy.get('.uib-tab').each(function($current){
//       cy.wrap($current).click();
//     })
//   });
// });
//-----------------------------------------------------------------

function waitPostValidate() {
  return cy.document()
    .then($document => {
      return new Cypress.Promise(resolve => { // Cypress will wait for this Promise to resolve
        const onQueryEnd = () => {
          $document.removeEventListener('postValidate', onQueryEnd) // cleanup
          resolve() // resolve and allow Cypress to continue
        }
        $document.addEventListener('postValidate', onQueryEnd)
      })
    });
}


function visit(currentPage) {
  // var currentPage = pages.pop();
  cy.visit(currentPage);
  return cy.get('bopform').then(() => {
    //cy.wait(2000);
    return waitPostValidate();
    //cy.matchScreenshot(currentPage);
    //return //waitPostValidate();
  }).then(() => {
    cy.matchScreenshot(currentPage);
    cy.log('Test done for: ' + currentPage);
    //if(pages.length) visit(pages);
  })
}

// var pages = [];
let pages = ["albaraka", "coreBON", "coreBONExternal", "coreRBM", "coreRBMExternal", "coreSADC", "coreSARB", "coreSARBExternal", "flow", "flow_MD", "invBMP", "invCCM", "invFBCC", "invFXM", "invFlow", "invUXP", "investec", "investecPL", "sasfin", "sbIBR1", "sbMW", "sbNA", "sbZA", "stdBON", "stdBankCommon", "stdBankLibra", "stdBankLibraNA", "stdBankNBOL", "stdSARB"];
// let pages = ["albaraka", "coreBON", "coreBONExternal"];


var waitForPages = () => {
  return new Cypress.Promise((resolve, reject) => {
    function checkPages() {
      if (pages.length == 0) {
        console.log('waiting');
        setTimeout(checkPages, 100);
      } else {
        resolve();
      }
    }
    checkPages();
  })
}

// describe('Test1', function () {
  // before(function(done) {
  //   // runs once before all tests in the block
  //   cy.task('readDir','dist/js/pack').then((pageRegistry)=>{
  //     pages = pageRegistry;
  //     console.log(pages);
  //     done();
  //   })
  // })


//   context('pages1', () => {
//     it('Gets the channels to test', function (done) {
//       cy.task('readDir', 'dist/js/pack').then((pageRegistry) => {
//         pages = pageRegistry;
//         console.log('pages:', pages);
//         done();
//       })
//     })
//   })

  





// })


describe('Page Tests', () => {

  // before('wait for pages', (done) => {
  //   waitForPages().then(() => {
  //     console.log("Doing page tests!", pages);
  //     done();
  //   });
  // })

  waitForPages().then(() => {
    console.log("Doing page tests!", pages);
    pages.forEach(page => {
      console.log("Doing page:", page);
      it(`Visits ${page}`, (done) => {
        visit(page).then(() => {
          done();
        });
      })
    })

  })
})


  // describe('Test2', function() {
  //   console.log("Doing page tests!");
  //   pages.forEach(page=>{
  //     it(`Visits ${page}`,(done)=>{
  //       visit(page);
  //       done();
  //     })
  //   })

  // })



