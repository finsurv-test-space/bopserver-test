
var gulp   = require('gulp');
var config = require('../config');


gulp.task('build', gulp.series(['build-ts', /*'styles',*/ 'vendor']));
gulp.task('watch', gulp.series(['watch-ts', 'watch-html', 'watch-less']));
gulp.task('dev', gulp.series(['watch', 'styles', 'vendor']));

gulp.task('watch-html', function () {
    gulp.watch(config.templates.html, ['templates']);
});

gulp.task('watch-less', function () {
    gulp.watch(config.styles.watch, ['styles']);
});