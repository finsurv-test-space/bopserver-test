
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var config = require('../config');
var minify = require('gulp-minify-css');

var pump = require('pump');

gulp.task('minify', gulp.series('minify-js'));

gulp.task('minify-js', function (cb) {

    //minifyJs(config.dev.root + config.scripts.dest + 'main.js');
    //minifyJs(config.dev.root + config.scripts.dest + 'lib.js');
    // gulp.src([config.dev.root + config.scripts.dest + 'lib.js', config.dev.root + config.scripts.dest + 'main.js'])
    //     .pipe(concat({ path: 'lib.js' }))
    //     .pipe(uglify()) 
    //     .pipe(gulp.dest(config.dist.root + config.scripts.dest));
    pump([
        gulp.src([config.dev.root + config.scripts.dest + 'lib.js', config.dev.root + config.scripts.dest + 'main.js']),
        uglify(),
        gulp.dest(config.dist.root + config.scripts.dest)
    ],cb)

});
