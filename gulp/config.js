'use strict';

module.exports = {
    'scripts': {
        'dest': 'js/',
        'vendor': [
          './node_modules/angular/angular.js',
          './node_modules/es6-promise/dist/es6-promise.auto.min.js',
          //'./node_modules/es5-shim/es5-shim.min.js',
          //'./node_modules/es6-shim/es6-shim.min.js',
            // Bundle almond.js after non r.js'ed resources...
            //'./node_modules/almond/almond.js',
            './node_modules/requirejs/require.js',
            // Bundle almond.js before  r.js'ed resources...
            "./electronic-bop-form/build/rules/coreRules/coreRules.min.js",
            "./electronic-bop-form/build/rules/evaluationEx/evaluationEx.min.js",
            "./electronic-bop-form/build/src/app.module.js"
        ]
    },
    'templates': {
        'html': './electronic-bop-form/src/partials/**/*.html',
        'file': 'templates.*',
        'dest': './src/',
        'options': {
            'module': 'bopForm',
            'standalone': false
        }
    },
    'styles': {
        'watch': './electronic-bop-form/src/css/**/*.less',
        'src': ['./electronic-bop-form/src/css/custom.less'],
        'dest': 'css/',
        'vendor': [
            './node_modules/bootstrap/dist/css/bootstrap.css'
        ]
    },
    'dev': {
        'root': './dev/'
    },
    'dist': {
        'root': './dist/',
    },
    'pack': {
        'root': './pack/',
    }
};
