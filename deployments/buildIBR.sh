#!/bin/bash

echo ''
echo 'Deleting Old forms'
echo ''

rm -r BOTTS/build
rm -r BITTS/build
rm -r RAVN/build

echo '========================================================================================================================================================'

echo ''
echo 'Building all channels and serving Electronic-Bop-Form on port 3010'
echo ''

echo '========================================================================================================================================================'

if npm run build; then

    npm start &
    sleep 2

    mkdir BOTTS/build
    mkdir BITTS/build
    mkdir RAVN/build

    echo ''
    echo '========================================================================================================================================================'
    echo ''
    echo ''

        # BOTTS 
    echo 'Getting BOTTS form'
    echo ''
    curl -o BOTTS/build/api.js http://localhost:3010/report-data-store/producer/api/form/js/sbIBR1
    echo ''
    echo ''

        # BITTS
    echo 'Getting BITTS form'
    echo ''
    curl -o BITTS/build/api.js http://localhost:3010/report-data-store/producer/api/form/js/sbIBR2
    echo ''
    echo ''

        # RAVN
    echo 'Getting RAVN form'
    echo ''
    curl -o RAVN/build/api.js http://localhost:3010/report-data-store/producer/api/form/js/sbRAVN
    echo ''
    echo ''

        # tXstream api.js form (for testing)
    # curl -o sbIBR/build/api.js http://localhost:3010/report-data-store/producer/api/barejs/devData


    cat BOTTS/extParams.js BOTTS/build/api.js > BOTTS/build/api.txt
    cat BITTS/extParams.js BITTS/build/api.js > BITTS/build/api.txt
    cat RAVN/extParams.js RAVN/build/api.js > RAVN/build/api.txt

    kill %1

else

    echo ''
    echo '========================================================================================================================================================'
    echo ''
    echo 'Script Failed!!!!!!!!!'
    echo ''
    echo '========================================================================================================================================================'
fi
