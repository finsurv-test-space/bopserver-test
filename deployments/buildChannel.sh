#!/bin/bash -e
set -e
#git submodule update --init --recursive

cd electronic-bop-form/rules
npm run buildChannels -- $1
cd ../..
cp electronic-bop-form/build/rules/crules/$1/pack.min.js dist/js/pack/$1/