/**
 * The last bit of code included in tsconfig.json
 *
 * This "bootstraps" the application.
 */

define("angular", function() {
  /**
   * This is for angular 1.x ... it is defined globally, so we just return it here so it can play with almond.
   */
  return angular;
});

var hasEvents = true;

try{
  new Event("Foo");
}catch{
  hasEvents = false;
}

interface ExtValidationParam {
  name: String;
  path: String;
}

interface ExtValidation {
  name: String;
  endpoint: String;
  parameters: Array<ExtValidationParam>;
  failOnError: String;
  failOnBusy: String;
}

function _emptyIfNotDefined(x, y) {
  return typeof x != "undefined" ? (x[y] ? x[y] : "") : "";
}

var _capture = { http: undefined, scope: undefined, $timeout: (cb:Function)=>{} },
  channelName, extraExtParams = {}, customURLTransform;
  // function(params){
  //   var out = [];
  //   for(var key in params){
  //     out.push(key+'='+params[key]);
  //   }
  //   return encodeURIComponent(out.join('&'))
  // };
var validationBaseUrl, rdsProxyPath;
var DEBUG = false;
var noExtCall = false;

var log = function(...args){
  if(DEBUG){
    console.log.apply(this, args);
  }
}

function mockResponse(resultFunc) {
  setTimeout(function() {
    resultFunc("pass", "This is a pass-through. Not a real pass.");
    // update the scope.
    _capture.scope.$digest();
  }, 1000);
}

function appendTransform(defaults, transform) {

  // We can't guarantee that the default transformation is an array
  defaults = angular.isArray(defaults) ? defaults : [defaults];

  // Append the new transformation to the defaults
  return defaults.concat(transform);
}

function makeRestCall(endPoint, params, resultFunc, failOnError) {
  var _params={};
  Object.assign(_params, extraExtParams, {channelName:channelName}, params);
  var callConfig = {
    url: validationBaseUrl + endPoint,
    method: 'GET',
    withCredentials: true,
    cache: false,
    data : '',
    headers: {
      'Content-Type': 'application/json'
     }
  };

  if(customURLTransform){
    callConfig.url = customURLTransform(callConfig.url, _params);
  }else{
    callConfig['params'] = _params;
  }

  return _capture.http(callConfig)
    .then(function(response) {
      if (response.data && (response.data.status || response.data.Status)) {
        var status = (response.data.status || response.data.Status).toLowerCase();
        var code = response.data.code || response.data.Code;
        var message = response.data.message || response.data.Message;
        resultFunc(
          status,
          code,
          message
        );
      }
      else if (response.data && !(response.data.status || response.data.Status))
        throw new Error("Server returned data but the response was malformed.");
      else {
        if(failOnError == "true" || failOnError == true){
          resultFunc("fail", "500", "Server did not return data.");
        }
        else{
          resultFunc("warning", "500", "Server did not return data.");
        }
      } 
      // update the scope.
      _capture.scope.$digest();
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
      if(failOnError == "true" || failOnError == true){
        resultFunc("fail", "500", "Server did not return data.");
      }
      else{
        resultFunc("warning", "500", "Server did not return data.");
      }
      // update the scope.
      _capture.scope.$digest();
    });
}

function makeExtCall(endPoint, params, resultFunc, failOnError) {
  if (DEBUG || noExtCall) {
    log(
      `Making External Call: ${endPoint} with params: ${JSON.stringify(params)}`
    );
    return mockResponse(function(type, code, msg) {
      log(
        `Called back with type ${type}, code ${code}, and msg '${msg}'`
      );
      resultFunc(type, code, msg);
    });
  } else {
    return makeRestCall(endPoint, params, resultFunc, failOnError);
  }
}

function getURLProps() {
  var parts = window.location.search.substr(1).split("&");
  var ret = {};
  parts.forEach(function(part) {
    var s = part.split("=");
    ret[s[0]] = s[1];
  });
  return ret;
}

var URLProps = getURLProps();

window['Validate_ImportUndertakingCCN'] = function(...args){
  var validationParams: Array<ExtValidation> =
  window["validationParams"];

  var validationParam = validationParams.find(itm=>{
    return itm.name=="Validate_ImportUndertakingCCN";
  })

  var params = validationParam.parameters.reduce(
    (memo, val, ind) => {
      memo[val.name.toString()] = args[ind];
      return memo;
    },
    {}
  );

  makeExtCall(
    validationParam.endpoint.toString(),
    params,
    (status, validationCode, validationMsg)=>{
      window['a'].setCustomData('LUClient',"N");
      if(status=="pass" && validationMsg.indexOf('registered')!=-1){
        window['a'].setCustomData('LUClient',"Y");
      }
      args[args.length - 3](status, validationCode, validationMsg);//third-last argument is the callback...
    },
    false
    
  );
}


window['Validate_ValidCCN'] = function(...args){
  var validationParams: Array<ExtValidation> =
  window["validationParams"];

  var validationParam = validationParams.find(itm=>{
    return itm.name=="Validate_ValidCCN";
  })

  var params = validationParam.parameters.reduce(
    (memo, val, ind) => {
      memo[val.name.toString()] = args[ind];
      return memo;
    },
    {}
  );

  makeExtCall(
    validationParam.endpoint.toString(),
    params,
    (status, validationCode, validationMsg)=>{
      window['a'].setCustomData('LUClient',"N");
      if(status=="pass" && validationCode && (validationCode == "200") ){
        window['a'].setCustomData('LUClient',"Y");
      }
      args[args.length - 3](status, validationCode, validationMsg);//third-last argument is the callback...
    },
    false
    
  );
}


//the final export...

window["getReportAPI"] = function(options, lookups, evaluation, validation, display, docRules) {
  if (!options) options = {};
  if (!options.config) options.config = {};
  if (!options.initData)
    options.initData = {
      customData: _emptyIfNotDefined(window["ReportInfo"], "Meta"),
      transaction: _emptyIfNotDefined(window["ReportInfo"], "Report")
    };

  if(options.noExtCall){
    noExtCall = true;
  }

  channelName = _emptyIfNotDefined(window["ReportInfo"], "ChannelName");
  // and the over-ride
  if(options.extraExtParams) extraExtParams = options.extraExtParams;
  if(options.customURLTransform) customURLTransform = options.customURLTransform;

  rdsProxyPath = options.reportAPIBaseURL
    ? options.reportAPIBaseURL
    : "report-service/";

  if(!options.config.baseURL)
    options.config.baseURL = rdsProxyPath;

  validationBaseUrl = options.extValidationBaseUrl
    ? options.extValidationBaseUrl
    : rdsProxyPath + "producer/api/validation/dynamic/";

  if (!options.config.validationRulesURL && !validation) {
    options.config.validationRulesURL =
      rdsProxyPath + "producer/api/rules/" + channelName + "/validation";
  }

  return new Promise((resolve, reject) => {
    require(["pack", "src/api"], function(pack, api) {
      /** Set up the external validations */
      function init(lookups) {
        if (window["validationParams"]) {
          var extValidationCallbackOptions = [];
          var validationParams: Array<ExtValidation> =
            window["validationParams"];
          for (var i = 0; i < validationParams.length; i++) {
            var validationParam = validationParams[i];
            //Create the callback function (This should be hard to read, because it was hard to write)
            window[validationParam.name.toString()] = 
            window[validationParam.name.toString()] ? window[validationParam.name.toString()]: (
              function(validationParam) { 
                return function(...args) {
                var params = validationParam.parameters.reduce(
                  (memo, val, ind) => {
                    memo[val.name.toString()] = args[ind];
                    return memo;
                  },
                  {}
                );

                params["channelName"] = channelName?channelName:URLProps["channelName"];

                makeExtCall(
                  validationParam.endpoint.toString(),
                  params,
                  args[args.length - 3],//third-last argument is the callback...
                  (validationParam.failOnError ? validationParam.failOnError.toString() == "true" : false)
                );
              };
            })(validationParam);

            var extValidationCallback = [
              validationParam.name.toString(),
              window[validationParam.name.toString()],
              {"failOnBusy": validationParam.failOnBusy}
            ];

            console.log("extValidationCallback " + extValidationCallback)
            validationParam.parameters.reduce((memo, val, ind) => {
              if (ind > 0) memo.push(val.path);
              return memo;
            }, extValidationCallback);

            extValidationCallbackOptions.push(extValidationCallback);
          }

          options.validationCallBacks = extValidationCallbackOptions;
        }
        var dependencies = ["bopForm"];
        if(options.config.extraDeps)
          dependencies=dependencies.concat(options.config.extraDeps);
        var bopPage = angular.module("bopPage",dependencies);

        bopPage.controller("bopPageCntrl", [
          "$timeout",
          "$scope",
          "$http",
          function($timeout, $scope, $http: angular.IHttpService) {
            // set security header if there is a JWT token supplied as a global, or passed in as an option.
            if (window["access_token"] || options.access_token) {
              $http.defaults.headers.common.Authorization =
                "Bearer " + (window["access_token"] || options.access_token);
            }
            _capture.http = $http;
            _capture.scope = $scope;
            _capture.$timeout = $timeout;
          }
        ]);

        var rulesPack = pack(lookups.lookups);
        if (docRules) rulesPack.documentRules = docRules;
        if (display) rulesPack.display = display;
        if (evaluation) rulesPack.evaluation = evaluation;
        if (validation) rulesPack.validation = validation;

        if(options.config && options.config.postValidate) {
          var _postValidate = options.config.postValidate;
          options.config.postValidate = function(...args){
            _postValidate.apply(this,args);
            _capture.$timeout(()=>{
              if(hasEvents)
                document.dispatchEvent(new Event('postValidate'));
            });
          }
        }else{
          options.config.postValidate = function(...args){
            _capture.$timeout(()=>{
              if(hasEvents)
                document.dispatchEvent(new Event('postValidate'));
            });
          }
        }

        document.addEventListener('postValidate',function(){console.log('postValidate done.')});

        var _api = new api.default(rulesPack, options, _capture);

        var _form = _api.newForm();

        _form.getEvaluator()
        .then((evaluator)=>{
          _api.evaluator =  evaluator;
          resolve(_api);
        });


        
      }

      // TODO: setup evaluation rules paths
      if (!lookups)
        require(["data/lookups"], function(lookups) {
          init(lookups);
        });
      else init(lookups);
    }, reject);
  });
};

// this is the version with everything just packed in.
window["getReportAPI_Packed"] = options => {
  return new Promise((resolve, reject) => {
    require([
      "data/lookups",
      "evaluation/evalRules",
      "evaluation/evalContext",
      "evaluation/evalScenarios",
      "validation/transaction",
      "validation/money",
      "validation/importexport",
      'document/transaction',
      'document/money',
      'document/importexport',
    ], function(
      lookups: any,
      evalRules: any,
      evalContext: any,
      evalScenarios: any,
      transaction: any,
      money: any,
      importexport: any,
      docTrans: any,
      docMoney: any,
      docIE: any
    ) {
      window["getReportAPI"](
        options,
        { lookups },
        { evalRules, evalScenarios, evalContext },
        { transaction, money, importexport },
        null,
        {trans:docTrans,money:docMoney,ie: docIE}
      ).then(api => {
        resolve(api);
      });
    });
  });
};
