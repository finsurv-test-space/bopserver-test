"use strict";
exports.__esModule = true;
/**
 * Created by petruspretorius on 22/11/2016.
 */
var ReportForm_1 = require("./ReportForm");
//import evaluationEx = require( "evaluationEx" );
var evaluationEx = require("evaluationEx");
var enums_1 = require("enums");
var enums = require("enums");
;
var ReportObject = /** @class */ (function () {
    function ReportObject() {
        this.decisions = [];
    }
    return ReportObject;
}());
var ReportingSide;
(function (ReportingSide) {
    ReportingSide[ReportingSide["cr"] = 'cr'] = "cr";
    ReportingSide[ReportingSide["dr"] = 'dr'] = "dr";
})(ReportingSide || (ReportingSide = {}));
/**
 * This is the API object for SARB reporting.
 */
var api = /** @class */ (function () {
    function api(rulePack, options) {
        this.enums = enums; //expose the enums.
        this.reportObject = new ReportObject();
        this.rulePack = rulePack;
        this.options = options;
        //load the templates...
        rulePack.partials();
        if (rulePack.evaluation.evalRules) {
            this.initEvalRules(rulePack.evaluation);
        }
    }
    api.prototype.initEvalRules = function (evaluation) {
        this.evaluator = evaluationEx.evaluator();
        var duplicateRules = this.evaluator.setup(evaluation.evalRules(evaluationEx), evaluation.evalAssumptions(evaluationEx), new Date());
    };
    // /** Gets the ReportObject */
    // getReportObject() {
    //     return this.reportObject;
    // }
    // /** Sets the ReportObject */
    // setReportObject(reportObject?: ReportObject) {
    //     this.reportObject = reportObject;
    // }
    /** Gets the ReportObject as a String */
    api.prototype.getReportObject = function () {
        delete this.reportObject.cr;
        delete this.reportObject.dr;
        if (this.CREDITForm)
            this.reportObject.cr = this.CREDITForm.getFormData();
        if (this.DEBITForm)
            this.reportObject.dr = this.DEBITForm.getFormData();
        return JSON.stringify(this.reportObject);
    };
    /** Sets the ReportObject */
    api.prototype.setReportObject = function (reportObject) {
        delete this.DEBITForm;
        delete this.CREDITForm;
        this.reportObject = JSON.parse(reportObject);
    };
    /**
     * Initialises a new form
     */
    api.prototype.newForm = function () {
        return new ReportForm_1.ReportForm(this.rulePack, this.options);
    };
    /**
      Retrieves the ReportForm object for the given reporting side. This ReportForm object can then be used to enrich data in the contained ReportObject and to show the form for further enrichment. There are “on-us” scenarios where both the Dr and Cr side need to be reported to the regulator and therefore would require two forms.
     */
    api.prototype.getReportFormForReportingSide = function (reportingSide) {
        var form = this[reportingSide + "Form"];
        if (!form) {
            //var options = angular.extend({initData:this.reportObject[enums.DrCr[reportingSide].toLowerCase()]},this.options)
            form = this[reportingSide + "Form"] = this.newForm();
            //form.setFormData(this.reportObject[enums.DrCr[reportingSide].toLowerCase()]);
        }
        return form;
    };
    /** Evaluate a transaction with unknown credit side
     * @param drResStatus - The residency status of the debit side
     * @param drAccType - The account type of the debit side
     * @param crResStatus - The residency status of the credit side
     * @param crAccType - The account type of the credit side
    */
    api.prototype.evaluate = function (drResStatus, drAccType, crResStatus, crAccType) {
        this.reportObject.decisions = this.evaluator.evaluateRaw(drResStatus, drAccType, crResStatus, crAccType);
    };
    /** Evaluate a transaction with unknown credit side
     * @param drResStatus - The residency status of the debit side
     * @param drAccType - The account type of the debit side
     * @param counterpartyStatus - Is the account holder an entity or an individual?
     * @param destinationCurrency - The transfer currency
     * @param accountHolderStatus -  Is the account holder a resident or a non-resident?
    */
    api.prototype.evaluateUnknownCrSide = function (drResStatus, drAccType, counterpartyStatus, destinationCurrency, accountHolderStatus) {
        this.reportObject.decisions = this.evaluator.evaluateUnknownCrSide(drResStatus, drAccType, counterpartyStatus, destinationCurrency, accountHolderStatus);
    };
    /** Evaluate a transaction with unknown debit side
     * @param crResStatus - The residency status of the credit side
     * @param crAccType - The account type of the credit side
     * @param counterpartyStatus - Is the account holder an entity or an individual?
     * @param sourceCurrency - The transfer currency
     * @param accountHolderStatus -  Is the account holder a resident or a non-resident?
     * @param field72 - Contents of field72 in the SWIFT message
    */
    api.prototype.evaluateUnknownDrSide = function (crResStatus, crAccType, counterpartyStatus, sourceCurrency, accountHolderStatus, field72) {
        this.reportObject.decisions = this.evaluator.evaluateUnknownDrSide(crResStatus, crAccType, counterpartyStatus, sourceCurrency, accountHolderStatus, field72);
    };
    /** Check if the reporting side has to report
     * @param reportingSide - the side that has to report
     */
    api.prototype.mustReportReportingSide = function (reportingSide) {
        var decisionlist = this.reportObject.decisions.filter ? this.reportObject.decisions : this.reportObject.decisions.decisions;
        var decision = decisionlist.filter(function (item) {
            return ((item.reportingSide == reportingSide)
                && ([enums_1.ReportableType.Reportable, enums_1.ReportableType.ZZ1Reportable].indexOf(item.reportable) !== -1));
        });
        return !!decision.length;
    };
    return api;
}());
exports["default"] = api;
