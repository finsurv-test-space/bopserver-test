"use strict";
exports.__esModule = true;
var app = require("app.module");
var angular = require("angular");
var ReportForm = /** @class */ (function () {
    function ReportForm(rulePack, options) {
        this.formData = {
            customData: {},
            transaction: {}
        };
        this.options = {};
        this.options = angular.extend({}, options);
        this.options.config = angular.extend({}, app.getConfig(), {
            noCss: true,
            rulePack: rulePack,
            partialsPath: "",
            fieldPartialsPath: "" //,
            //css: '../custom/STDBank/css/custom',
        }, this.options.config);
    }
    /**
     * Initialises the formData with the current transactionData.
     */
    ReportForm.prototype.setData = function () {
        this.options.initData = (this.formData);
    };
    /** Sets the form data  */
    ReportForm.prototype.setFormData = function (formData) {
        this.formData = formData;
        this.setData();
    };
    /** Gets the form data  */
    ReportForm.prototype.getFormData = function () {
        return this.formData;
    };
    /** Sets the custom data  */
    ReportForm.prototype.replaceCustomData = function (customData) {
        this.formData.customData = customData;
        this.setData();
    };
    /** Sets the transaction data  */
    ReportForm.prototype.setTransactionData = function (formData) {
        this.formData.transaction = formData;
        this.setData();
    };
    /** Replaces the core transaction data, leaving the Resident, NonResident and MonetaryAmount intact */
    ReportForm.prototype.replaceCoreTransactionData = function (coreTransactionData) {
        var ResData = this.formData.transaction.Resident;
        var NonResData = this.formData.transaction.NonResident;
        var MoneyData = this.formData.transaction.MonetaryAmount;
        this.formData.transaction = coreTransactionData;
        this.formData.transaction.Resident = ResData;
        this.formData.transaction.NonResident = NonResData;
        this.formData.transaction.MonetaryAmount = MoneyData;
        this.setData();
    };
    /** Replaces the Resident data only */
    ReportForm.prototype.replaceResidentData = function (residentData) {
        this.formData.transaction.Resident = residentData;
        this.setData();
    };
    /** Replaces the NonResident data only */
    ReportForm.prototype.replaceNonResidentData = function (nonresidentData) {
        this.formData.transaction.NonResident = nonresidentData;
        this.setData();
    };
    /** Replaces the Money data only */
    ReportForm.prototype.replaceMonetaryAmount = function (monetaryData) {
        this.formData.transaction.MonetaryAmount = monetaryData;
        this.setData();
    };
    /** Display the form */
    ReportForm.prototype.showForm = function (container, saveCB) {
        app.initOptions(this.options, function () {
            var body = container ? container : document.getElementsByTagName('body')[0];
            body.innerHTML = "";
            var _container = document.createElement('div');
            _container.setAttribute('ng-controller', 'bopPageCntrl');
            _container.appendChild(document.createElement('bopform'));
            body.appendChild(_container);
            angular.bootstrap(_container, ['bopPage']);
        });
    };
    /** Is the form valid? */
    ReportForm.prototype.isValid = function () {
        return app.isValid();
    };
    /** Does the form have warnings? */
    ReportForm.prototype.hasWarning = function () {
        return app.hasWarning();
    };
    /** Get the warning count */
    ReportForm.prototype.getWarningCount = function () {
        return app.getWarningCount();
    };
    /** Get the error count */
    ReportForm.prototype.getErrorCount = function () {
        return app.getErrorCount();
    };
    /** Get a list of the raised events. */
    ReportForm.prototype.getRaisedEvents = function () {
        return app.getRaisedEvents();
    };
    /** get the data */
    ReportForm.prototype.getData = function (isTransactionOnly) {
        return app.getData(isTransactionOnly);
    };
    /** Set custom data directly */
    ReportForm.prototype.setCustomData = function (key, val) {
        app.setCustomData(key, val);
    };
    return ReportForm;
}());
exports.ReportForm = ReportForm;
