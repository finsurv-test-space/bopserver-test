
import app = require("app.module");
import * as angular from 'angular';

// declare module angular {
//     export function extend(...args: any[]): any;
//     export function bootstrap(...args: any[]): any;
// };

declare function require(moduleNames: string[], onLoad: (...args: any[]) => void): void;


//import * as angular from "../../node_modules/angular/angular";
export interface rulePack_T {
  data: {
    lookups: any
  },
  display: {
    detailDisplay: any,
    lookupFilterRules: any,
    summaryDisplay: any,
  },
  evaluation: any,
  formDefinition: any,
  partials: Function,
  predef: any,
  validation: {
    importexport: any,
    money: any,
    transaction: any,
  },
  //info:{mappings:}
}

export interface Options {
  config?: {
    noCss?: boolean;
    css?: string;
    partialsPath?: string;
    fieldPartialsPath?: string;
    validationRulesURL?: string;
    displayRulesURL?: string;
    lookupsURL?: string;
    formDefinitionURL?: string;
    customRulesPath?: string;
    postValidate?: (isValid:boolean)=>void;
  }
  initData?: any;
}

interface CustomData {

}

interface TransactionData {
  Resident?: any,
  NonResident?: any,
  MonetaryAmount?: any[]
}

export interface FormData {
  customData?: CustomData, //internal
  transaction?: TransactionData, //internal
  Meta?: CustomData, //external
  Report?: TransactionData, //external
}

export class ReportForm {

  formData: FormData = {
    customData: {},
    transaction: {}
  };

  options: Options = {
    // local config for tXstream BOP form.

  };

  http: any;

  constructor(rulePack: rulePack_T, options?: Options, http?:any) {
    this.http = http;
    this.options = angular.extend({}, options);
    this.options.config = angular.extend({}, app.getConfig(), {
      noCss: true,
      rulePack: rulePack,
      partialsPath: "",
      fieldPartialsPath: ""//,
      //css: '../custom/STDBank/css/custom',
    }, this.options.config);
    this.formData = this.options.initData?this.options.initData:{};
  }

  private dataTrans(formData){
    return (formData.Meta || formData.Report)? {
      customData: formData.Meta,
      transaction: formData.Report,
    } : formData;
  }

  /**
   * Initialises the formData with the current transactionData.
   */
  private setData() {
    this.options.initData = (this.dataTrans(this.formData));
  }

  replaceData(formData: FormData){
    app.setData(this.dataTrans(formData));
  }

  /** Sets the form data for init  */
  setFormData(formData: FormData) {
    this.formData = this.dataTrans(formData);
    this.setData();
  }

  /** Gets the form data  */
  getFormData(): FormData {
    return {
      Meta: this.formData.customData,
      Report: this.formData.transaction
    };
  }

  /** Sets the custom data  */
  replaceCustomData(customData: CustomData) {
    this.formData.customData = customData;
  }

  /** Sets the transaction data  */
  setTransactionData(formData: TransactionData) {
    this.formData.transaction = formData;
  }

  /** Replaces the core transaction data, leaving the Resident, NonResident and MonetaryAmount intact */
  replaceCoreTransactionData(coreTransactionData: TransactionData) {
    let ResData = this.formData.transaction.Resident;
    let NonResData = this.formData.transaction.NonResident;
    let MoneyData = this.formData.transaction.MonetaryAmount;

    this.formData.transaction = coreTransactionData;

    this.formData.transaction.Resident = ResData;
    this.formData.transaction.NonResident = NonResData;
    this.formData.transaction.MonetaryAmount = MoneyData;

  }

  /** Replaces the Resident data only */
  replaceResidentData(residentData) {
    this.formData.transaction.Resident = residentData;
  }

  /** Replaces the NonResident data only */
  replaceNonResidentData(nonresidentData) {
    this.formData.transaction.NonResident = nonresidentData;

  }

  /** Replaces the Money data only */
  replaceMonetaryAmount(monetaryData) {
    this.formData.transaction.MonetaryAmount = monetaryData;

  }

  getEvaluator(){
    return new Promise((resolve,reject)=>{
      app.initOptions(null, this.options, () => {
        resolve(app.getEvaluator())
      })
    })
  }

  /** Display the form */
  showForm(container?: HTMLElement, saveCB?: Function) {

    container = container ? container : document.getElementsByTagName('body')[0];
    
    var _self = this;
    // set up the callback for post validation (we can assume options.config exists at this point)
    if(this.options.config.postValidate){
      var tempPostValidate = this.options.config.postValidate;
    }
    this.options.config.postValidate = (isValid)=>{
      if(tempPostValidate) tempPostValidate(isValid);
      if(saveCB){
        saveCB(_self.isValid(),_self.getErrorCount(),_self.getWarningCount(),_self.getRaisedEvents())
      }
    }
    
    app.initOptions(container, this.options, () => {
      container.innerHTML = "";
      let _container = document.createElement('div')
      _container.setAttribute('ng-controller', 'bopPageCntrl');
      _container.appendChild(document.createElement('bopform'));
      container.appendChild(_container);
      try{
        angular.bootstrap(_container, ['bopPage']);
      } catch(e) {
        throw e;
        //angular.module('app').requires.push('bopPage');
      }
    })
  }

  submit(url?: String) {
    var data = this.getData();
    return this.http.http.post(
      //rdsProxyPath + "/producer/api/upload/reports/sync/sarb?channelName=" + channelName,
      url ? url : rdsProxyPath + "producer/api/js?channelName=" + channelName,
      data
    )
  }

  /** Just validate the data and return the raised events */
  validate(data: any) {
    var _self = this;
    return new Promise((resolve)=>{
      app.initOptions(undefined, _self.options,()=>{
        resolve(app.validateData(this.dataTrans(data)));
      });
    })
  }

  /** Is the form valid? */
  isValid() {
    return app.isValid();
  }

  /** Does the form have warnings? */
  hasWarning() {
    return app.hasWarning();
  }

  /** Get the warning count */
  getWarningCount() {
    return app.getWarningCount();
  }

  /** Get the error count */
  getErrorCount() {
    return app.getErrorCount();
  }

  /** Get a list of the raised events. */
  getRaisedEvents() {
    return app.getRaisedEvents();
  }

  /** get the data */
  getData(isTransactionOnly?: boolean) {
    let data = app.getData(isTransactionOnly);
    return isTransactionOnly?data:{
      Meta: data.customData,
      Report: data.transaction,
    }
  }

  /** Set custom data directly */
  setCustomData(key: String, val: any): void {
    app.setCustomData(key, val);
  }
}