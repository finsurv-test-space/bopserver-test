
var formData,
  isValid,
  iFrameReady,
  iFrameID = "ID_OF_I_FRAME_GOES_HERE";
  iframe = document.getElementById(iFrameID);
// message handler
window.onmessage = function (e) {
  if (e.data && e.data.indexOf('data:') === 0) {
    formData = JSON.parse(e.data.substr(5));
  } else if (e.data && e.data.indexOf('isValid:') === 0) {
    var val = e.data.substr(8);
    isValid = val === 'true' ? true : false;
  } else if (e.data && e.data.indexOf('ready') === 0) {
    iFrameReady = true;
  } else if (e.data && e.data.indexOf('resize:') === 0) {
    var match = /^resize:(\w+):(\d+)/g.exec(e.data);
    var size = Number(match[2]);
    var frame = window.document.getElementsByName(
      match[1]
    )[0];
    if (frame && !Number.isNaN(size)) {
      frame.height = size + 10 + 'px';
    }
  }
};


function waitForIFrame() {
  function _waitForIFrame(resolve) {
    return function () {
      if (iFrameReady) {
        resolve(iFrameReady);
      } else {
        setTimeout(_waitForIFrame(resolve), 500);
      }
    };
  }
  return new Promise((resolve, reject) => {
    _waitForIFrame(resolve)();
  });
}

function setData(data) {
  iframe.contentWindow.postMessage(
    "setData:" + JSON.stringify({
      customData: data.Meta,
      transaction: data.Report
    }), '*'
  );
}


function getData() {
  delete formData;
  iframe.contentWindow.postMessage('getData', '*');
  function waitForData(resolve) {
    return function () {
      if (formData) {
        resolve({
          Meta: formData.customData,
          Report: formData.transaction
        });
      } else {
        setTimeout(waitForData(resolve), 500);
      }
    };
  }
  return new Promise((resolve, reject) => {
    waitForData(resolve)();
  });
}