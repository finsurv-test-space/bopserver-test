/**
 * Created by petruspretorius on 22/11/2016.
 */

//import 'es5-shim';
//import 'es6-shim';

import { resStatus, accType, cpInstitution, accStatus, DrCr, ReportableType, EvaluationDecision } from 'enums';
import *  as enums from 'enums';

import { ReportForm, rulePack_T, Options, FormData } from "./ReportForm";

declare module angular {
    export function extend(...args: any[]): any;
    export function bootstrap(...args: any[]): any;
};

class ReportObject {
    decisions: Array<EvaluationDecision> = [];
    cr: FormData;
    dr: FormData;
}

enum ReportingSide {
    cr = <any>'cr',
    dr = <any>'dr'
}


/**
 * This is the API object for SARB reporting.
 */
export default class api {
    enums = enums;//expose the enums.
    private reportObject: ReportObject = new ReportObject();
    private DEBITForm: ReportForm;
    private CREDITForm: ReportForm;
    private rulePack: rulePack_T;
    private options: Options;
    private http: any;
    private evalContext: any;
    private evaluator: any;

    constructor(rulePack: rulePack_T, options?: Options, http?: any) {
        this.rulePack = rulePack;
        this.options = options;
        this.http = http;
        //load the templates...
        rulePack.partials();
        


        // if (rulePack.evaluation.evalRules) {
        //     this.initEvalRules(rulePack.evaluation);
        // } else if (typeof rulePack.evaluation == 'function') {
        //     var evals = rulePack.evaluation(evaluationEx);
        //     this.evaluator = evaluationEx.evaluator();
        //     var duplicateRules = this.evaluator.setup(evals.rules, evals.scenarios[0], new Date(), evals.context);
        //     console.log('duplicate rules:',duplicateRules);
        // }
    }

    validate(data) {
        return this.newForm().validate(data);
    }

    // initEvalRules(evaluation: any) {
    //     this.evaluator = evaluationEx.evaluator();
    //     this.evalContext = evaluation.evalContext(evaluationEx);
    //     var duplicateRules = this.evaluator.setup(evaluation.evalRules(evaluationEx), evaluation.evalScenarios(evaluationEx), new Date(), this.evalContext);
    // }


    // /** Gets the ReportObject */
    // getReportObject() {
    //     return this.reportObject;
    // }

    // /** Sets the ReportObject */
    // setReportObject(reportObject?: ReportObject) {
    //     this.reportObject = reportObject;
    // }

    /** Gets the ReportObject as a String */
    getReportObject() {
        delete this.reportObject.cr;
        delete this.reportObject.dr;
        if (this.CREDITForm) this.reportObject.cr = this.CREDITForm.getData();
        if (this.DEBITForm) this.reportObject.dr = this.DEBITForm.getData();
        return JSON.stringify(this.reportObject);
    }

    /** Sets the ReportObject */
    setReportObject(reportObject?: string) {
        delete this.DEBITForm;
        delete this.CREDITForm;
        this.reportObject = JSON.parse(reportObject);
    }

    /**
     * Initialises a new form
     */
    newForm(): ReportForm {
        return new ReportForm(this.rulePack, this.options, this.http);
    }


    /**
      Retrieves the ReportForm object for the given reporting side. This ReportForm object can then be used to enrich data in the contained ReportObject and to show the form for further enrichment. There are “on-us” scenarios where both the Dr and Cr side need to be reported to the regulator and therefore would require two forms.
     */
    getReportFormForReportingSide(reportingSide: ReportingSide): ReportForm {
        var form = this[reportingSide + "Form"];
        if (!form) {
            //var options = angular.extend({initData:this.reportObject[enums.DrCr[reportingSide].toLowerCase()]},this.options)
            form = this[reportingSide + "Form"] = this.newForm();
            //form.setFormData(this.reportObject[enums.DrCr[reportingSide].toLowerCase()]);
        }
        return form;
    }

    /** Evaluate a transaction with unknown credit side 
     * @param drResStatus - The residency status of the debit side
     * @param drAccType - The account type of the debit side
     * @param drAccountHolderStatus - The account holders status of the debit side
     * @param crResStatus - The residency status of the credit side
     * @param crAccType - The account type of the credit side
     * @param crAccountHolderStatus - The account holders status of the cebit side
    */
    evaluateCustomerOnUs(drResStatus: resStatus, drAccType: accType,
        drAccountHolderStatus: accStatus,
        crResStatus: resStatus, crAccType: accType,
        crAccountHolderStatus: accStatus) {
        this.reportObject.decisions = this.evaluator.evaluateCustomerOnUs(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    }

    /** Evaluate a transaction with unknown credit side - returns only the most relevant results, if any.
     * @param drResStatus - The residency status of the debit side
     * @param drAccType - The account type of the debit side
     * @param drAccountHolderStatus - The account holders status of the debit side
     * @param crResStatus - The residency status of the credit side
     * @param crAccType - The account type of the credit side
     * @param crAccountHolderStatus - The account holders status of the cebit side
    */
    evaluateCustomerOnUsFiltered(drResStatus: resStatus, drAccType: accType,
        drAccountHolderStatus: accStatus,
        crResStatus: resStatus, crAccType: accType,
        crAccountHolderStatus: accStatus) {
        this.reportObject.decisions = this.evaluator.evaluateCustomerOnUsFiltered(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    }

    /** Evaluate a transaction with unknown credit side - returns only the most onerous Dr results
     * @param drResStatus - The residency status of the debit side
     * @param drAccType - The account type of the debit side
     * @param drAccountHolderStatus - The account holders status of the debit side
     * @param crResStatus - The residency status of the credit side
     * @param crAccType - The account type of the credit side
     * @param crAccountHolderStatus - The account holders status of the cebit side
    */
    evaluateCustomerOnUsDrOnerous(drResStatus: resStatus, drAccType: accType,
        drAccountHolderStatus: accStatus,
        crResStatus: resStatus, crAccType: accType,
        crAccountHolderStatus: accStatus) {
        this.reportObject.decisions = this.evaluator.evaluateCustomerOnUsDrOnerous(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    }

    /** Evaluate a transaction with unknown credit side - returns only the most onerous Cr results
     * @param drResStatus - The residency status of the debit side
     * @param drAccType - The account type of the debit side
     * @param drAccountHolderStatus - The account holders status of the debit side
     * @param crResStatus - The residency status of the credit side
     * @param crAccType - The account type of the credit side
     * @param crAccountHolderStatus - The account holders status of the cebit side
    */
    evaluateCustomerOnUsCrOnerous(drResStatus: resStatus, drAccType: accType,
        drAccountHolderStatus: accStatus,
        crResStatus: resStatus, crAccType: accType,
        crAccountHolderStatus: accStatus) {
        this.reportObject.decisions = this.evaluator.evaluateCustomerOnUsCrOnerous(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    }

    /** Evaluate a transaction with unknown credit side - returns only the Dr results
     * @param drResStatus - The residency status of the debit side
     * @param drAccType - The account type of the debit side
     * @param drAccountHolderStatus - The account holders status of the debit side
     * @param crResStatus - The residency status of the credit side
     * @param crAccType - The account type of the credit side
     * @param crAccountHolderStatus - The account holders status of the cebit side
    */
    evaluateCustomerOnUsDrFiltered(drResStatus: resStatus, drAccType: accType,
        drAccountHolderStatus: accStatus,
        crResStatus: resStatus, crAccType: accType,
        crAccountHolderStatus: accStatus) {
        this.reportObject.decisions = this.evaluator.evaluateCustomerOnUsDrFiltered(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    }

    /** Evaluate a transaction with unknown credit side - returns only the Cr results
     * @param drResStatus - The residency status of the debit side
     * @param drAccType - The account type of the debit side
     * @param drAccountHolderStatus - The account holders status of the debit side
     * @param crResStatus - The residency status of the credit side
     * @param crAccType - The account type of the credit side
     * @param crAccountHolderStatus - The account holders status of the cebit side
    */
    evaluateCustomerOnUsCrFiltered(drResStatus: resStatus, drAccType: accType,
        drAccountHolderStatus: accStatus,
        crResStatus: resStatus, crAccType: accType,
        crAccountHolderStatus: accStatus) {
        this.reportObject.decisions = this.evaluator.evaluateCustomerOnUsCrFiltered(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    }

    /** Evaluate a transaction with unknown credit side - returns the most relevant Dr decision only.
         * @param drResStatus - The residency status of the debit side
         * @param drAccType - The account type of the debit side
         * @param beneficiaryBankStatus_or_BIC - The counterparty's Insitution or BIC
         * @param destinationCurrency - The transfer currency
         * @param accountHolderStatus -  Is the account holder a resident or a non-resident?
        */
    evaluateCustomerPayment(drResStatus: resStatus, drAccType: accType,
        beneficiaryBankStatus_or_BIC: cpInstitution | String, destinationCurrency: string,
        accountHolderStatus: accStatus) {
        this.reportObject.decisions = this.evaluator.evaluateCustomerPayment(drResStatus, drAccType,
            beneficiaryBankStatus_or_BIC, destinationCurrency,
            accountHolderStatus);
    }

    /** Evaluate a transaction with unknown credit side 
     * @param drResStatus - The residency status of the debit side
     * @param drAccType - The account type of the debit side
     * @param beneficiaryBankStatus_or_BIC - The counterparty's Insitution or BIC
     * @param destinationCurrency - The transfer currency
     * @param accountHolderStatus -  Is the account holder a resident or a non-resident?
    */
    evaluateCustomerPaymentEx(drResStatus: resStatus, drAccType: accType,
        beneficiaryBankStatus_or_BIC: cpInstitution | String, destinationCurrency: string,
        accountHolderStatus: accStatus) {
        this.reportObject.decisions = this.evaluator.evaluateCustomerPaymentEx(drResStatus, drAccType,
            beneficiaryBankStatus_or_BIC, destinationCurrency,
            accountHolderStatus);
    }

    /** Evaluate a transaction with unknown credit side - returns only the Dr decisions
     * @param drResStatus - The residency status of the debit side
     * @param drAccType - The account type of the debit side
     * @param beneficiaryBankStatus_or_BIC - The counterparty's Insitution or BIC
     * @param destinationCurrency - The transfer currency
     * @param accountHolderStatus -  Is the account holder a resident or a non-resident?
    */
    evaluateCustomerPaymentDrFiltered(drResStatus: resStatus, drAccType: accType,
        beneficiaryBankStatus_or_BIC: cpInstitution | String, destinationCurrency: string,
        accountHolderStatus: accStatus) {
        this.reportObject.decisions = this.evaluator.evaluateCustomerPaymentDrFiltered(drResStatus, drAccType,
            beneficiaryBankStatus_or_BIC, destinationCurrency,
            accountHolderStatus);
    }

    /** Evaluate a transaction with unknown debit side - returns only the most relevant Cr decision
     * @param crResStatus - The residency status of the credit side
     * @param crAccType - The account type of the credit side
     * @param orderingBankStatus_or_BIC - The counterparty's Insitution or BIC
     * @param sourceCurrency - The transfer currency
     * @param accountHolderStatus -  Is the account holder a resident or a non-resident?
     * @param field72 - Contents of field72 in the SWIFT message
    */
    evaluateCustomerReceipt(crResStatus: resStatus, crAccType: accType,
        orderingBankStatus_or_BIC: cpInstitution | String, sourceCurrency: string,
        accountHolderStatus: accStatus,
        field72: string) {
        this.reportObject.decisions = this.evaluator.evaluateCustomerReceipt(crResStatus, crAccType,
            orderingBankStatus_or_BIC, sourceCurrency,
            accountHolderStatus, field72);

    }

    /** Evaluate a transaction with unknown debit side 
     * @param crResStatus - The residency status of the credit side
     * @param crAccType - The account type of the credit side
     * @param orderingBankStatus_or_BIC - The counterparty's Insitution or BIC
     * @param sourceCurrency - The transfer currency
     * @param accountHolderStatus -  Is the account holder a resident or a non-resident?
     * @param field72 - Contents of field72 in the SWIFT message
    */
    evaluateCustomerReceiptEx(crResStatus: resStatus, crAccType: accType,
        orderingBankStatus_or_BIC: cpInstitution | String, sourceCurrency: string,
        accountHolderStatus: accStatus,
        field72: string) {
        this.reportObject.decisions = this.evaluator.evaluateCustomerReceiptEx(crResStatus, crAccType,
            orderingBankStatus_or_BIC, sourceCurrency,
            accountHolderStatus, field72);

    }

    /** Evaluate a transaction with unknown debit side - returns only the Cr decisions
     * @param crResStatus - The residency status of the credit side
     * @param crAccType - The account type of the credit side
     * @param orderingBankStatus_or_BIC - The counterparty's Insitution or BIC
     * @param sourceCurrency - The transfer currency
     * @param accountHolderStatus -  Is the account holder a resident or a non-resident?
     * @param field72 - Contents of field72 in the SWIFT message
    */
    evaluateCustomerReceiptCrFiltered(crResStatus: resStatus, crAccType: accType,
        orderingBankStatus_or_BIC: cpInstitution | String, sourceCurrency: string,
        accountHolderStatus: accStatus,
        field72: string) {
        this.reportObject.decisions = this.evaluator.evaluateCustomerReceiptCrFiltered(crResStatus, crAccType,
            orderingBankStatus_or_BIC, sourceCurrency,
            accountHolderStatus, field72);

    }

    /**
     * 
     * @param orderingBIC - The ordering BIC
     * @param beneficiaryBIC - The beneficiary BIC
     * @param destinationCurrency - The destination Currency
     */
    evaluateBankPayment(orderingBIC: string, beneficiaryBIC: string, destinationCurrency: string) {
        this.reportObject.decisions = this.evaluator.evaluateBankPayment(orderingBIC, beneficiaryBIC,
            destinationCurrency);
    }

    /**
     * 
     * @param beneficiaryBIC - The beneficiary BIC
     * @param orderingBIC - The ordering BIC
     * @param sourceCurrency - The source Currency
     */
    evaluateBankReceipt(beneficiaryBIC: string, orderingBIC: string, sourceCurrency: string) {
        this.reportObject.decisions = this.evaluator.evaluateBankReceipt(beneficiaryBIC, orderingBIC,
            sourceCurrency);
    }


    consolidatedEvaluation(
      drBankBIC: string, drResStatus: resStatus, drAccType: accType,
      drCurrency: string, drField72: string,
      drAdditionalParams: any,
      crBankBIC: string, crResStatus: resStatus, crAccType: accType, crCurrency: string,
      crAdditionalParams: any,
      sideFiltered: string, mostOnerousFiltered: boolean
    ) {
      var decisionslist = this.evaluator.consolidatedEvaluation(drBankBIC, drResStatus, drAccType,
        drCurrency, drField72,
        drAdditionalParams,
        crBankBIC, crResStatus, crAccType, crCurrency,
        crAdditionalParams,
        sideFiltered, mostOnerousFiltered);
        this.reportObject.decisions = decisionslist.decisions ? decisionslist.decisions : decisionslist;
    }

    /**
     * 
     * @param issueingCountry - The issueing Country
     * @param useCountry - The counry of use
     */
    evaluateCARD(issueingCountry: string, useCountry: string) {
        this.reportObject.decisions = this.evaluator.evaluateCARD(issueingCountry, useCountry);
    }

    /** Check if the reporting side has to report
     * @param reportingSide - the side that has to report
     * @param institutionBIC - (Optional) the BIC of the institution matching the reporting side
     * @returns true if the reporting side must report
     */
    mustReportReportingSide(reportingSide: DrCr, institutionBIC: String): boolean {
        if(!this.reportObject.decisions) return false;
        let isItUs = institutionBIC && this.evalContext?institutionBIC.match(this.evaluator.evalContext.whoAmIRegex):true;
        if(!isItUs) return false;
        var useContext = function(context){
            return function(item){
                return (
                    (item.reportingSide) &&
                    (item.reportable) && 
                    (item.reportingSide == reportingSide) &&
                    /*([ReportableType.Reportable, ReportableType.ZZ1Reportable].indexOf(item.reportable) !== -1)*/
                    
                    ( 
                        ([ReportableType.Reportable].indexOf(item.reportable) !== -1) || 
                        ( 
                            (!context || context.ZZ1Reportability) && 
                            ([ReportableType.ZZ1Reportable].indexOf(item.reportable) !== -1) 
                        )
                    )
                )
            }
        };
        let decision = this.reportObject.decisions.filter(useContext(this.evaluator.evalContext));

        return !!decision.length;
    }

}









